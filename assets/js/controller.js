angular.module('myApp').controller('appController', function($scope, $window, appService) {

    //variables
    $scope.seatsConfig = appService.seatsConfig;
    $scope.seats = [];
    $scope.bookings = [];
    $scope.currentSelectedSeats = [];
    $scope.showSeatingArragement = false;    

    //methods

    //store seat information in local storage
    $scope.setSeatInfo = function() {
    	
    	if($window.localStorage.getItem('seats_info') === null) {    	
   			
    		for(var i = 0; i < $scope.seatsConfig.rows.length; i++) {
    			var obj = {};
    			for (var j= 0; j < $scope.seatsConfig.columns.length; j++) {
    				var obj = {
    					isAvailable: true,    					
    					row: $scope.seatsConfig.rows[i],
    					column: $scope.seatsConfig.columns[j],
    					seatId: $scope.seatsConfig.rows[i] + $scope.seatsConfig.columns[j]
    				}
    				$scope.seats.push(obj);
    			}    			
    		}
    		$window.localStorage.setItem('seats_info', JSON.stringify($scope.seats));
    	} else {    		
    		$scope.seats = JSON.parse($window.localStorage.getItem('seats_info'));
    	}   	
    };

    //set bookings info
    $scope.setBookingsInfo = function() {

    	if($window.localStorage.getItem('bookings_info') === null) {
    		$window.localStorage.setItem('bookings_info', JSON.stringify($scope.bookings));
    	} else {
    		$scope.bookings = JSON.parse($window.localStorage.getItem('bookings_info'));    		
    	}   	
    };

    //toggle selection of seats
    $scope.toggleSeatState = function(id) {    

    	if($scope.currentSelectedSeats.length < $scope.booking_info.seatsCount) {
    		for(var i = 0; i < $scope.seats.length; i++) {
	    		var currentSeat = $scope.seats[i];
	    		if( currentSeat.seatId === id && currentSeat.isAvailable === true ) {  
	    			var index = $scope.currentSelectedSeats.indexOf(currentSeat.seatId);  	  	
	    			if(index < 0) {    				
	    				$scope.currentSelectedSeats.push(currentSeat.seatId);
	    			} else {    		    				
	    				$scope.currentSelectedSeats.splice(index, 1);
	    			}    		
	    		}
	    	}    
    	}    	   
    };

    //check if seat selected
    $scope.isSeatSelected = function(id) {    
		return $scope.currentSelectedSeats.indexOf(id) > -1;     
    };

    //submit selected seats
    $scope.startSelection = function() { 
    	$scope.showSeatingArragement = true;   				
    };

    //submit selected seats
    $scope.submitSelectedSeats = function() { 

    	var obj = {
			name: $scope.booking_info.name,
			seats: $scope.currentSelectedSeats.join(',')
		}
		$scope.bookings.push(obj);
    	$window.localStorage.setItem('bookings_info', JSON.stringify($scope.bookings)); 
    	
    	for(var i = 0; i < $scope.seats.length; i++) {
    		if( $scope.currentSelectedSeats.indexOf($scope.seats[i].seatId) > -1) {
    			$scope.seats[i].isAvailable = false;
    		}
    	}
    	$window.localStorage.setItem('seats_info', JSON.stringify($scope.seats)); 
	   
    };

    $scope.isSeatAvailable = function(seatId) {
    	for(var i = 0; i < $scope.seats.length; i++) {
    		if($scope.seats[i].seatId === seatId) {
    			return $scope.seats[i].isAvailable;
    		}
    	}
    }

    $scope.reset = function() {
    	$window.location.reload();
    }


    $scope.setSeatInfo();
    $scope.setBookingsInfo();
  
});